package com.example.iamhere;

import android.Manifest;
import android.content.Intent;
import androidx.annotation.NonNull;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import static com.karumi.dexter.Dexter.*;

public class MainActivity extends AppCompatActivity {
    Button bSignUp, bSignIn;
    TextInputLayout etEmail, etPassword;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bSignUp = findViewById(R.id.bSignUp);
        bSignIn =findViewById(R.id.bSignIn);
        etEmail =findViewById(R.id.etEmail);
        etPassword =findViewById(R.id.etPassword);
        mAuth = FirebaseAuth.getInstance();

        withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();

        SharedPreferences setting = getSharedPreferences(getPackageName() + "UserDetails", MODE_PRIVATE);
        String userID = setting.getString("UserID", null);

        if (!(userID==null)){
            startActivity(new Intent(MainActivity.this, Main2Activity.class));
            finish();
        }



        bSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, SignUpActivity.class);
                MainActivity.this.startActivity(myIntent);

            }
        });
        bSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                formValidation();
            }
        });

    }

    private void formValidation() {
        String Email = etEmail.getEditText().getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!Email.matches(emailPattern)){
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
            return;
        }
        String Password = etPassword.getEditText().getText().toString().trim();
        if (!(Password.length() >0)){
            Toast.makeText(this, "Please Provide Password", Toast.LENGTH_SHORT).show();
            return;            
        }
        
        signIn(Email, Password);
    }

    private void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            SharedPreferences setting = getSharedPreferences(getPackageName() + "UserDetails", MODE_PRIVATE);
                            SharedPreferences.Editor editor = setting.edit();
                            editor.putString("UserID", user.getUid());
                            editor.putString("UserEmail", user.getEmail());
                            editor.commit();

                            startActivity(new Intent(MainActivity.this, Main2Activity.class));
                            finish();


                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });

    }

}
