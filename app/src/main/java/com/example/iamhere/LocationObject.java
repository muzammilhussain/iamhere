package com.example.iamhere;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
class LocationObject {
    public Double lat,lng;

    public LocationObject() {

    }

    public LocationObject(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }


}
