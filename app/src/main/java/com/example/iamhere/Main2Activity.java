package com.example.iamhere;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.Locale;

public class Main2Activity extends AppCompatActivity implements LocationListener {
    TextView tLocation;
    Button bEmergency;
    LocationManager locationManager;
    FirebaseDatabase database;
    DatabaseReference myRef;
    Switch switch_service;
    private static final int PERMISSION_CAMERA = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        bEmergency = findViewById(R.id.bEmergency);
        tLocation = findViewById(R.id.tLocation);
        switch_service = findViewById(R.id.switch_service);
        database = FirebaseDatabase.getInstance();
        SharedPreferences setting = getSharedPreferences(getPackageName() + "UserDetails", MODE_PRIVATE);
        String userID = setting.getString("UserID", "");
        myRef = database.getReference("users/" + userID);
        myRef.child("email").setValue(setting.getString("UserEmail", ""));

        if(ForegroundService.timer != null){
            switch_service.setChecked(true);
        }

        bEmergency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(Main2Activity.this, Scanner.class);
                Main2Activity.this.startActivity(mainIntent);
            }
        });

        switch_service.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(Main2Activity.this, "Please Provide Location Permission", Toast.LENGTH_SHORT).show();
return;
//            getDeviceIMEI();
                    }
                    Intent serviceIntent = new Intent(Main2Activity.this, ForegroundService.class);
                    serviceIntent.putExtra("inputExtra", "Foreground Service Example in Android");
                    ContextCompat.startForegroundService(Main2Activity.this, serviceIntent);


                } else {

                    if(ForegroundService.timer != null){
                        ForegroundService.timer.cancel();
                        ForegroundService.timer.purge();
                    }

                    Intent serviceIntent = new Intent(Main2Activity.this, ForegroundService.class);
                    stopService(serviceIntent);
                }
            }
        });


        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

//            getDeviceIMEI();
        }
    }

    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Write a message to the database

        Toast.makeText(this, location.toString(), Toast.LENGTH_SHORT).show();
        myRef.child("lat").setValue(location.getLatitude());
        myRef.child("lng").setValue(location.getLongitude());

        tLocation.setText("Latitude: " + location.getLatitude() + "\nLongitude: " + location.getLongitude());

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            myRef.child("address").setValue(addresses.get(0).getAddressLine(0) + ", " +
                    addresses.get(0).getAddressLine(1));
            tLocation.setText(tLocation.getText() + "\n" + addresses.get(0).getAddressLine(0) + ", " +
                    addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2));
        } catch (Exception e) {

        }


    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(Main2Activity.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
