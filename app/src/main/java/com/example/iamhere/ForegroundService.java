package com.example.iamhere;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ForegroundService extends Service  implements LocationListener {

    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private TimerTask timerTask;
    private Handler handler = new Handler();
    public static Timer timer;
    DatabaseReference myRef;
    LocationManager locationManager;
    FirebaseDatabase database;



    @Override
    public void onCreate() {
        super.onCreate();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
        database = FirebaseDatabase.getInstance();
        SharedPreferences setting = getSharedPreferences(getPackageName() + "UserDetails", MODE_PRIVATE);
        String userID = setting.getString("UserID", "");
        myRef = database.getReference("users/" + userID);
        myRef.child("email").setValue(setting.getString("UserEmail", ""));
        sendMyLocation();



        return START_NOT_STICKY;
    }

    public void sendMyLocation(){
        getLocation();


//        timer = new Timer();
//        timerTask = new TimerTask() {
//            public void run() {
//                handler.post(new Runnable() {
//                    public void run(){
//                        //your code is here
//                        Toast.makeText(ForegroundService.this, "chl le yaar", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//        };
//        timer.schedule(timerTask, 5000, 5000);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // Write a message to the database

        Toast.makeText(this, location.toString(), Toast.LENGTH_SHORT).show();
        LocationObject locationObject = new LocationObject(location.getLatitude(),location.getLongitude());
        myRef.child("history").push().setValue(locationObject);


        //set lat and lng separately also
        myRef.child("lat").setValue(location.getLatitude());
        myRef.child("lng").setValue(location.getLongitude());


//        tLocation.setText("Latitude: " + location.getLatitude() + "\nLongitude: " + location.getLongitude());

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            myRef.child("address").setValue(addresses.get(0).getAddressLine(0) + ", " +
                    addresses.get(0).getAddressLine(1));
//            tLocation.setText(tLocation.getText() + "\n" + addresses.get(0).getAddressLine(0) + ", " +
//                    addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2));
        } catch (Exception e) {

        }


    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(ForegroundService.this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
