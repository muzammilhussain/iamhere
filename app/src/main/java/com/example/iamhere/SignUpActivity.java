package com.example.iamhere;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {
    Button bSignUp;
    TextInputLayout etEmail, etPassword,etName,etContact,etOrganization,etEmgContact;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference myRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        bSignUp = findViewById(R.id.bSignUp);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etName = findViewById(R.id.etName);
        etContact = findViewById(R.id.etContact);
        etEmgContact = findViewById(R.id.etEmgContact);
        etOrganization = findViewById(R.id.etOrganization);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();


        bSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formValidation();

            }
        });
    }

    private void formValidation() {
        String Email = etEmail.getEditText().getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!Email.matches(emailPattern)) {
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
            return;
        }
        String Password = etPassword.getEditText().getText().toString().trim();
        if (!(Password.length() > 0)) {
            Toast.makeText(this, "Please Provide Password", Toast.LENGTH_SHORT).show();
            return;
        }

        signUp(Email, Password);
    }

    private void signUp(final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SignUpActivity.this, "Sign Up Successfully", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
//                  save in sharedpreferences
                            SharedPreferences setting = getSharedPreferences(getPackageName() + "UserDetails", MODE_PRIVATE);
                            SharedPreferences.Editor editor = setting.edit();
                            editor.putString("UserID", user.getUid());
                            editor.putString("UserEmail", user.getEmail());
                            editor.commit();
                            myRef = database.getReference("users/" + user.getUid());
                            myRef.child("email").setValue(etEmail.getEditText().getText().toString());
                            myRef.child("username").setValue(etName.getEditText().getText().toString());
                            myRef.child("contact").setValue(etContact.getEditText().getText().toString());
                            myRef.child("emergencyContact").setValue(etEmgContact.getEditText().getText().toString());
                            myRef.child("organization").setValue(etOrganization.getEditText().getText().toString());

                            startActivity(new Intent(SignUpActivity.this, Main2Activity.class));
                            finish();
//                            SharedPreferences setting = getSharedPreferences(getPackageName()+"UserDetails", MODE_PRIVATE);
//                            e=setting.getString("UserEmail", "");
                        } else {
                            Toast.makeText(SignUpActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}
